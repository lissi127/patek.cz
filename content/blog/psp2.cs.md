---
title: "Páteční Sled Přednášek (25. 5. 2020)"
date: 2020-05-24T22:10:32+02:00
draft: false
authors: [ "Greenscreener" ]
tags: [ "psp" ]
---
{{< button href="https://meet.jit.si/P%C3%A1tek" text="Připojit se k přednášce" >}}
<br>

[Livestream na YouTube](https://www.youtube.com/watch?v=wHAd9gOFr7k)

Ahoj všichni, opět chystáme na pondělí další Páteční Sled Přednášek. Bude se opět odehrávat **v podnělí 25. 5. 2020 od 14:00**, přednášky už máme odhlasované a opět by měly vyjít na **60 - 90 minut**. Znovu se potkáme na [Jitsi](https://meet.jit.si/Pátek) a dostupný bude také [livestream na YouTube](https://youtu.be/wHAd9gOFr7k).

Pokud jste tak ještě neučili a chcete, máte možnost se přihlásit k odběru novinek o těchto akcích [zde](https://forms.gle/SJpUP9XvicqN8ZBo6).

Teď už ale k tomu co nás bude čekat, **program** bude následující:

14:00 - **Franta Kmječ** - Datové struktury (základy programování, díl druhý)

~14:30 - **Vojta Káně** - Otevřenost, FOSS a decentralizace s praktickou ukázkou   

~15:00 - **Lenka Kopfová** - Teorie her
 
~15:30 - začátek volné diskuse


