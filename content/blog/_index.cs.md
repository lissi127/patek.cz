---
title: "Blog"
buttonColor: "#FFDC00"
buttonTextColor: "black"
menu: 
  main: 
    weight: 3
  landing:
    weight: 2
    
---
V blogu najdete všechny články, které se nám nevešly do ostatních sekcí, zápisy z Pátků, oznámení, výlety, návody, vykecávání se o tom, na čem zrovna pracujeme a mnoho dalšího. 
