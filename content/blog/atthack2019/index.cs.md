---
title: "AT&T Hackaton 2019"
date: 2019-11-30T10:47:08+01:00
draft: false
authors: [ "Greenscreener" ]
tags: []
---
Od pátku do soboty se minulý týden konal AT&T Hackaton 2019 v pavilonu E brněnského výstaviště. Na akci jsme samozřejmě vyjeli, i když jsme se domnívali, že nemáme moc velkou šanci. Do moravské metropole jsme dojeli už ve čtvrtek k večeru a strávili jsme večer poznáváním místní kultury. Snažili jsme se přijít na to, co že to na oněm hackatonu budeme stavět, ale nic nás nenapadlo. 

Po spánku o dost kratším, než by bylo vhodné, jsme vyrazili směrem k výstavišti. Před budovou už stál kamion „Fablab experience“, pojízdná dílna brněnského hackerspacu Fablab, který má na palubě mimo jiné elektronový mikroskop, laserovou řezačku, 3D tiskárny, plotter a mnoho dalšího. Po registraci jsme se usadili ke stolu na samém konci místnosti a pokračovali jsme v tom, co jsme dělali hned od začátku. Snažili jsme se přijít na to, co že to budeme sakra stavět. 

{{< blogPhoto src="fablab-experience.jpg" alt="FabLab experience" >}}

Také jsme postupně navazovali kontakty s organizátory, prvním byl Michal Hrušecký, jeden z autorů Turrisu, který si všiml, že jsme si na akci přivezli vlastní Omnii. A samozřejmě to zveřejnil i na Twitter:

{{< tweet 1197796476728086530 >}} 

Proběhlo několik prezentací od předních českých odborníků v oboru hardwaru i softwaru a my jsme stále neměli nejmenší tušení, v čem spočívá naše „million dollar idea“. A tak jsme dále konverzovali, poslouchali přednášky, navštěvovali raut plný luxusního jídla, když v tom odbila dvanáctá hodina a soutěž začala. 

Od začátku soutěže panovala, k našemu vlastnímu překvapení, jaksi nesoutěžní atmosféra. Nezdálo se, že by soutěžící přijeli vyhrát za každých okolností, organizátoři a mentoři si s každým rádi povídali, chodili okolo stolů soutěžících a ptali se, jestli nemohou nějak pomoci, radili, když si někdo nevěděl s něčím rady. To neplatilo pouze pro mentory, jakožto soutěžící jsme si také rádi pomáhali. 

{{< blogPhoto src="atthack-contest.jpg" alt="Probíhající soutěž" >}}

Všechny součástky, které jsme si mohli přát, byly na místě také dostupné k zapůjčení. Když jsme přišli s požadavkem na DMX konektor a světlo, na chvíli vyvalil oči, ale pak prohlásil, že „má jedno doma a až brácha dojede domů, tak mu ho přiveze.“ 

To ale trochu předbíhám. První dvě hodiny soutěže jsme trávili čas konverzací mezi sebou, ale také s mentory, abychom konečně vymysleli nějaký kloudný nápad. Po asi třech hodinách konečně vyplavala na povrch první známka smysluplného plánu.

Všechna zvuková a osvětlovací technika je drahá, proprietární a velmi obtížná na ovládání a i k malé akci je potřeba několik velkých pultů, které dokáží ovládat pouze profesionálové, ale z vlastní zkušenosti víme, že se správným nástrojem to zvládne ovládat i laik. Rozhodli jsme se vyrobit zařízení, do kterého může uživatel zapojit všechny kabely do zvuku, promítání, světel a na mobilu si otevře jednoduchou aplikaci, ze které bude možné vše ovládat. 

To ale nebylo vše. Součástí naší aplikace byla možnost vytvoření „scénáře“, který vypadá stejně, jako jakýkoliv divadelní nebo filmový scénář, s tím rozdílem, že obsahuje tlačítka, která obsahují akce jako „přehraj video“, „přehraj zvuk“ nebo „rozsviť světlo“. Po kliknutí se akce sama vykoná. Proto k ovládání stačí pouze sledovat, co se děje na pódiu a klikat na správná tlačítka, když přijde jejich čas.

Tak jsme si rozdělili práci a začali jsme makat, neboť jsme věděli, že máme ještě hodně práce před sebou. Šimon se ponořil hlavou napřed do webové aplikace ve Vue, Vojta dělal na backendu v Go, Kyslík s Adamem vyráběli krabičku a design a já jsem se vrtal v Arduinu na ovládání DMX.

Od té doby jsme zuřivě pracovali až do 12:00 následujícího dne. Samozřejmě jsme si dělali přestávky na návštěvu rautu, sdílení memes s organizátory na Discordu nebo také několik koleček okolo nočního výstaviště. 

Po skončení 24hodinového maratonu jsme ale museli náš projekt odprezentovat. Celá soutěž včetně této prezentace probíhala v angličtině, což jsme ale hravě zvládli. Prezentace, která musela obsahovat živou demonstraci, proběhla téměř hladce, až na chvíli, kdy si porota vyžádala předvedení jedné z funkcí. Po kliknutí na tlačítko místo kýžené akce zmizela polovina uživatelského rozhraní aplikace, což naštěstí přijala porota s pochopením. 

{{< blogPhoto src="atthack-presentation.jpg" alt="Prezentace produktu" >}}

Poté nastal čas vyčkávání. Ten jsme strávili užíváním si úžasné atmosféry, konverzací s mentory, konzumací delikates z rautu, hraním videoher na konzolích v relax zóně a někteří také spánkem. 

S blížícím se vyhlášením se začal vzduch plnit očekáváním. Během prezentací nám došlo, že možná budeme mít šanci nebýt poslední a že se nám podařilo vytvořit docela ucházející produkt. Kyslík to okomentoval: „Hlavně ať nejsme čtvrtý, to by naštvalo nejvíc.“ 

Moderátor měl skutečně smysl pro napětí a podařilo se mu vyhlášení prvních tří míst protáhnout tak, že jsme měli pocit, že trvá hodiny. Bylo vyhlášeno třetí místo, druhé místo a najednou jsme slyšeli: „And in the first place, the project nicknamed 'Show Controller', from team Pátek!“

Nemohli jsme tomu uvěřit, ale skutečně to tak bylo. Vyhráli jsme. 

{{< blogPhoto src="atthack-win.jpg" alt="Vítězný tým s diplomem" >}}

Poté už nás čekalo pouze rozloučení s mentory, úklid a dlouhá cesta směrem k domovu. 

Děkujeme moc všem mentorům, organizátorům a ostatním soutěžícím za skvělou akci.


- [Gitlabový repozitář projektu](https://gitlab.com/patek-devs/psc)
- [Hackster.io článek o projektu](https://www.hackster.io/patek/patek-show-controller-a1bfc7)
