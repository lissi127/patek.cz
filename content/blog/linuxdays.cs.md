---
title: "Linuxdays"
date: 2019-08-27T15:06:07+02:00
draft: false
authors: [ "Greenscreener" ]
tags: [ "vylet" ]
---
Jako každý rok se 5. a 6. října konají na fakultě informačních technologií ČVUT v Praze LinuxDays - největší česká linuxácká konference. Už poněkolikáté jedeme v malé skupince poslouchat přednášky nejlepších z nejlepších v české komunitě linuxáků.