---
title: "Roboti, seminárky, robosoutěž"
date: 2019-10-06T11:44:18+02:00
draft: false
authors: [ "Greenscreener" ]
tags: [ "schuze" ]
---
Na Pátku 04.10.2019 jsme byli znovu přítomni v hojném počtu. 

Nejmladší členové z primy programovali roboty Edison pomocí programovacího
[prostředí](https://meetedison.com/robot-programming-software/edscratch/), 
které je podobné jazyku [scratch](https://scratch.mit.edu). Za všudypřítomného 
pípání se všude po místnosti pohybovaly malé oranžové kostičky, tancující dle 
přednastavených programů, sledující čáru nebo narážející do všeho, co jim stálo 
v cestě. 

Jako obvykle přišel [Petr Kospach](/authors/ok1ven), konzultoval se studenty
sexty jejich seminární práce, pomáhal s opravou teslova transformátoru a
jako obvykle předával své mnohé zkušenosti. 

Jeden z našich týmů začal pilně pracovat na svém robotu na
[Robosoutěž](https://robosoutez.fel.cvut.cz). Letošní úloha je překonat
simulované pohoří na hrací ploše a to jako obvykle pouze pomocí plně
samostatného robota vytvořeného z lega.

Jako každý Pátek ale také probíhalo mnoho diskusí na mnoho různých témat, jedním
tématem bylo například použití rekurze v implementaci faktoriálu a Fibonacciho
posloupnosti.

Založili jsme také Pátečnický [twitter](https://twitter.com/patekvpatek).

