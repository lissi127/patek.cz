---
title: "Blog"
buttonColor: "#FFDC00"
buttonTextColor: "black"
menu: 
  main: 
    weight: 3
  landing:
    weight: 2
---
The blog contains all the articles we couldn't fit into other sections including what happened during recent Páteks, announcements, trips, guides, articles about our projects and much more.