---
title: "Homepage"
date: 2019-09-08T11:10:18+02:00
draft: false
authors: [ "Greenscreener" ]
---
We are a group of students interested not only in IT and programming but also physics, electrical engineering, amateur radio, math, biology and anything interesting in general.
