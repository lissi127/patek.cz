---
title: "Projects"
buttonColor: "#0F87D7"
menu: 
  main: 
    weight: 16
  landing:
    weight: 3
---
Members of Pátek are a creative bunch, we like to make stuff, bodge stuff and generally mess around. We learn new things and waste time that could be used for much more valuable activities. One of the reasons we had for creating this website was our desire to share our experiences with these projects with other members as well as with unsuspecting members of the public. If you try hard enough, you might even find some interesting information about ongoing and past projects. 

