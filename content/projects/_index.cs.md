---
title: "Projekty"
buttonColor: "#0F87D7"
menu: 
  main: 
    weight: 16
  landing:
    weight: 3
---
Pátkaři jsou tvořivá sebranka, rádi bastlíme, kutíme a vyrábíme. Učíme se tím novým dovednostem a mrháme časem, který bychom jinak strávili něčím užitečnějším. Jedním z důvodů pro vznik těchto stránek byla naše touha podělit se o zkušenosti nabyté v rámci těchto projektů s ostatními Pátečníky, ale také s *normálními* lidmi. Když se tedy budete dostatečně snažit, možná zde najdete nějaké zajímavé informace o proběhlých a probíhajících projektech.