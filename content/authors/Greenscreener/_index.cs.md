---
title: "Greenscreener"
avatar: "https://greenscreener.tk/images/GSLOGO.svg"
fullname: "Jan Černohorský"
---
Ahoj, jsem Greenscreener, vlastním jménem Jan Černohorský, jsem dlouholetý člen Pátku a také člen výkonné rady Pátek z. s. Jsem jedním z autorů těchto stránek a také [gbl.cz](https://gbl.cz). Jsem nadšený linuxák a většinu svého volného času trávím tvorbou webových aplikací. Ke svému vlastnímu znechucení stále nejlépe ovládám JavaScript, ale píšu i v Pythonu, bashi a podobných. Hrozně se mi líbí Go, ale ještě jsem neměl dostatek trpělivosti na to, abych se ho pořádně naučíl. Rád si hraju s Arduinem a elektronikou, často ve škole 3D tisknu. Občas dokonce něco vyrobím v Blenderu. 

Najdete mě na [Twitteru](https://twitter.com/GrnScrnr), [GitHubu](https://github.com/Greenscreener), [Telegramu](https://t.me/grnscrnr) a více informací o mně najdete na mých webových stránkách [greenscreener.tk](https://greenscreener.tk).
