---
title: "O nás"
date: 2019-07-12T14:54:15+02:00
draft: false
authors: [ "Greenscreener" ]
buttonColor: "#1CA620"
menu: 
  main: 
    weight: 19
  landing:
    weight: 1
---
Zatímco pátek je pátý den v týdnu, Pátek je kroužek a skupina studentů, kteří se zajímají nejen o informatiku, ale také fyziku, elektrotechniku, radioamatérství, matematiku, biologii a vše co je zajímavé. Stejně jako se [pátečníci](https://cs.wikipedia.org/wiki/P%C3%A1te%C4%8Dn%C3%ADci) Karla Čapka scházeli v jeho vile každé páteční odpoledne, tak se také my scházíme každé páteční odpoledne v jedné či více učebnách brandýského gymnázia poté, co všichni příčetní studenti odejdou domů. Velkou část těchto schůzek trávíme diskusemi o absurdních tématech (např. teorie myšlenky, antimyšlenky a jejich vzájemné anihilace), hádáním se o kontroverzních názorech (např. proč je Linux bezkonkurenčně lepší oproti ostatním operačním systémům) či prací na čemkoliv co, zrovna daný člen nestíhá. Děláme ale také mnoho *bohulibějších* činností:

### Přednášky PátekTalks
Dle známého českého přísloví *„Kdo umí, umí, kdo neumí, učí.“* si členové, kteří vědí něco, co ostatní neví, připravují jednou za čas přednášky o tématech, která jim připadají zajímavá. Vzhledem k tomu, že většina starších členů Pátku jsou skalní informatici, se většina přednášek ubírá tímto směrem, avšak kdo má odvahu, může se jim postavit se svým tématem.  

### Projekty
Pátkaři jsou tvořivá sebranka, rádi bastlíme, kutíme a vyrábíme. Učíme se tím novým dovednostem a mrháme časem, který bychom jinak strávili něčím užitečnějším. Jedním z důvodů pro vznik těchto stránek byla naše touha podělit se o zkušenosti, nabyté v rámci těchto projektů, s ostatními Pátečníky, ale také s *normálními* lidmi. Když se tedy budete dostatečně snažit, možná zde najdete nějaké zajímavé informace o proběhlých i probíhajících projektech.

### Výlety
Když se naskytne možnost, rádi vyjíždíme na mnohé výlety za vědou, např. za přednáškovou akcí [100vědců akademie věd ČR](http://100vedcu.cz). Další výlety [zde](/tags/vylet).

### PátekPrint
Gymnázium získalo minulý rok 3D tiskárnu Prusa Reprap i3 Mk3, kterou mají na starosti čtyři členové Pátku. Na Pátcích se proto téměř neustále tiskne a jakýkoli Pátkař má možnost si za pomoci zkušenějších členů cokoliv vymodelovat a následně vytisknout. Většinou ale tiskneme blbinky, co si lidi najdou na [thingiversu](https://thingiverse.com).

### Programování
Jak už bylo zmíněno výše, velká část Pátečníků jsou informatici, kteří tráví svůj volný čas civěním do monitorů svých počítačů na barevná písmenka, která nedávají smysl. Kromě vyhrocených debat o nesmyslech také tvoří [stránky&nbsp;gymnázia](https://gbl.cz) a [tyto&nbsp;stránky](/). Weby to ale nekončí, jedním z momentálně pozastavených projektů je Gomber - openSource implementace BomberMana napsaná v Go, na které se učíme základy tohoto krásného programovacího jazyka. 

### PátekSpace
Někteří by naše uskupení dokonce nazvali malý [hackerspace](https://cs.wikipedia.org/wiki/Hackerspace). Jednou z hlavních výhod hackerspaců je dostupnost nástrojů, které jednotliví členové nemohou mít doma. Zatím máme pouze 3D tiskárnu a pár šroubováků, ale po předchozí dohodě si občas půjčujeme některé laboratorní vybavení gymnázia (samozřejmě pod dohledem učitele 😉). Postupem času ale plánujeme získat více nástrojů, které budou k dispozici všem členům.

<br />
<br />
<br />

Hlavně jsme ale parta kamarádů a děláme, co nás baví. Doufám, že Vám návštěva našich stránek přinese něco užitečného a nakonec už nezbývá, než Vám popřát příjemný den.

Za všechny členy Pátku,

[Greenscreener](/authors/greenscreener)
