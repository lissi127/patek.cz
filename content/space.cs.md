---
title: "Space"
date: 2019-09-08T12:54:22+02:00
draft: false
authors: [ "Greenscreener" ]
buttonColor: "#33007f"
menu: 
  main:
    weight: 10
  landing:
    weight: 6
    title: "<patek-logo title='Space' subtitle='Pátek'>Space</patek-logo>"
---
Někteří by naše uskupení dokonce nazvali malý [hackerspace](https://cs.wikipedia.org/wiki/Hackerspace). Jednou z hlavních výhod hackerspaců je dostupnost nástrojů, které jednotliví členové nemohou mít doma. Zatím máme pouze 3D tiskárnu a pár šroubováků, ale po předchozí dohodě si občas půjčujeme některé laboratorní vybavení gymnázia (samozřejmě pod dohledem učitele 😉). Postupem času ale plánujeme získat více nástrojů, které budou k dispozici všem členům.
