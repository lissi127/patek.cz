---
title: "Print"
date: 2019-09-08T12:45:22+02:00
draft: false
authors: [ "Greenscreener" ]
buttonColor: "#ff7300"
buttonTextColor: "black"
menu:
  main:
    weight: 9
  landing:
    weight: 5
    title: "<patek-logo title='Print' subtitle='Pátek'>Print</patek-logo>"
---
Gymnázium získalo minulý rok 3D tiskárnu Prusa Reprap i3 Mk3, kterou mají na starosti čtyři členové Pátku. Na Pátcích se proto téměř neustále tiskne a jakýkoli Pátkař má možnost si za pomoci zkušenějších členů cokoliv vymodelovat a následně vytisknout. Většinou ale tiskneme blbinky, co si lidi najdou na [thingiversu](https://thingiverse.com).

<a target="_blank" href="https://docs.google.com/forms/d/e/1FAIpQLSeBGasvDgj937T58yLhyG4WUVP7DMUAVAZYNjg5YkKqj1Ulaw/viewform">Objednávací formulář pro tisk</a>
