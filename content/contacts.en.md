---
title: "Contact"
menu:
  main:
    weight: 20
---
<!--
# Pátek, z. s.
ulice ČP

město PSČ

IČ: 00000000
-->

email: [patek@gbl.cz](mailto:patek@gbl.cz)

Chairman: [Vojtěch Káně](mailto:vojtech.kane@gbl.cz)

Members of the executive board:
[Jan Černohorský](mailto:jan.cernohorsky@gbl.cz),
[Eva Kospachová](mailto:e.kospachova@gbl.cz),
[Tomáš Kysela](mailto:tomas.kysela@gbl.cz),
[Šimon Šustek](mailto:simon.sustek@gbl.cz)
