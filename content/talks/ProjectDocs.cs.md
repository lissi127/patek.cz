---
title: "Projektová dokumentace"
date: 2019-09-09T09:26:58+02:00
draft: false
authors: ["Kyslík"]
when: 2019-09-06T15:00:00+02:00
---
Projektová dokumentace je klíčovým prostředkem pro komunikaci nejen vývojářů s uživateli, avšak i mezi jednotlivými vývojáři, či v rámci jednho projektu. Proto jsme se nejdříve zaměřili na rozdělení jednotlivých typů dokumentace (interní, externí). Následovala krátká ukázka povedených a nepovedených dokumentací, jež byla provázena povídáním o správném psaní dokumentací a zakončeno prostorem pro dokumentaci.