---
title: "Git"
date: 2020-04-14T10:53:44+02:00
draft: false
authors: [ "Greenscreener", "Vojta" ]
when: 2020-04-15T17:00:00+02:00
slides:
  - data: http://talks.grnscrnr.tk/git
recordings:
  - data: https://www.youtube.com/watch?v=rlSiRVewDu0
---

Git je nejrozšířenější VCS současnosti. Co to vlastně je? Jak se používá? Během této přednášky chceme představit git úplným začátečníkům, představit základní, ale i složitější koncepty práce s ním a jak (ne)zbořit celý repozitář jedním příkazem. 

Přednáška by měla proběhnout online na [Jitsi](https://meet.vpsfree.cz/P%C3%A1tek).
