---
title: "Kybersoutěž a PátekCTF"
date: 2019-09-08T14:13:58+02:00
draft: false
authors: [ "sijisu", "Greenscreener" ]
when: 2019-09-06T15:15:00+02:00
---
Kybersoutěž je česká soutěž v kyberbezpečnosti a „hackování“, pořádaná mnoha českými úřady, které se zabývají touto problematikou. Oba jsme se této soutěže účastnili a [Šimon](/authors/sijisu) je dokonce členem národního týmu. V tomto Talku jsme mluvili o naší účasti v této soutěži, nabádali jsme ostatní Pátkaře k účasti a také jsme vyprávěli o našich zkušenostech s kyberbezpečností. 

Dalším tématem bylo PátekCTF, malá „hackerská“ soutěž, kterou [Šimon](/authors/sijisu) zorganizoval pro Pátečníky. Obsahovala několik jednoduchých úloh, na kterých si mohli Pátkaři vyzkoušet, jak obvykle podobné soutěže fungují. V přednášce jsme prošli všechny úlohy a jejich příkladové řešení. 