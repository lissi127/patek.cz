---
title: "Talks"
date: 2019-08-06T09:43:00+02:00
draft: false
authors: [ "unknown" ]
when: 2019-08-06T09:43:00+02:00
buttonColor: "red"
navColor: "red"
menu: 
  main: 
    weight: 8
  landing:
    weight: 4
    title: "<patek-logo title='Talks' subtitle='Pátek'>Talks</patek-logo>"
---
In the spirit of a known Czech saying, that can be roughly translated to *"Who is good at something does it, who is bad at something teaches it."* students, who know something the others don't, prepare talks about topics they consider interesting. Because most members are conservative computer scientists, most these talks follow this direction, but if there is anyone brave enough, they can stand up to them with their topics. 

Obviously, we would be delighted, if anyone outside our little group joined us with their talk. 

This page contains write-ups for most of the talks we do. Some are only available in [Czech](/talks).