---
title: "Úvod do UNIXu"
date: 2019-10-25T08:30:00+02:00
draft: false
authors: ["Vojta"]
when: 2019-10-11T14:00:00+02:00
---
Talk o UNIXu, POSIXu a Linuxu, o tom, co mají společného, co který termín vlastně znamená a taky, že to je jedno, protože v praxi se spolu často zaměňují.

Následně jsme se spolu podívali na přehled verzí POSIXu na [wiki](https://en.wikipedia.org/wiki/POSIX), rámcově zjistili, co vynucují a jak je to implementováno v Linuxu.

Bude-li zájem, navážu v budoucnosti přehledem již konkrétně o shellu a POSIXových utilitách.

Přednáška byla inspirována výborným předmětem [NSWI095 Úvod do UNIXu](https://is.cuni.cz/studium/predmety/index.php?do=predmet&kod=NSWI095) pana [RNDr. Libora Forsta](https://www.ms.mff.cuni.cz/~forst/).
