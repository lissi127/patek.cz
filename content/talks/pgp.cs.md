---
title: "OpenPGP: každý je certifikační autorita"
date: 2020-05-18T16:29:43+02:00
draft: false
authors: [ "Sijisu" ]
when: 2020-05-18T15:00:00+02:00
slides:
  - data: https://present.sijisu.eu/personal/openpgp
recordings:
  - data: https://www.youtube.com/watch?v=TSiyKBtUSLI
---
V půlhodině nám Šimon shrnul základy kryptografie, především rozdíl mezi symmetrickými a asymmetrickými šiframi, a ukázal, jak se s tím vypořádává OpenPGP, otevřená alternativa Pretty Good Privacy Phila Zimmermanna.
