lint:
	docker-compose run xo-js-lint
	docker-compose run xo-html-lint
	docker-compose run sass-lint

build:
	docker-compose run xo-js-lint
	docker-compose run xo-html-lint
	docker-compose run sass-lint
	hugo

serve: 
	hugo server &
