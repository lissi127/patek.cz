document.addEventListener("DOMContentLoaded", () => {
	const headings = document.querySelectorAll(".content h1, .content h2, .content h3, .content h4, .content h5, .content h6");
	headings.forEach(e => {
		const a = document.createElement("a");
		a.href = "#" + e.id;
		a.innerText = "#";
		a.classList.add("header-link");
		e.prepend(a);
	});
});
