document.addEventListener("DOMContentLoaded", () => {
	if (window.location.search.indexOf("darkmode=false") > -1) {
		document.cookie = "darkmode=false; path=/";
		if (document.getElementById("content") !== null) {
			document.getElementById("content").classList.remove("dark-mode");
		}
	} else if (window.location.search.indexOf("darkmode=true") > -1 || document.cookie.indexOf("darkmode=true") > -1) {
		document.cookie = "darkmode=true; path=/";
		if (document.getElementById("content") !== null) {
			document.getElementById("content").classList.add("dark-mode");
		}
	}
});
