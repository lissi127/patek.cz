document.addEventListener("DOMContentLoaded", () => {
	document.addEventListener("scroll", () => {
		if (window.scrollY > document.querySelector("#home-navbar .menu-placeholder").offsetTop) {
			document.getElementById("home-navbar").classList.add("is-fixed-top");
		} else {
			document.getElementById("home-navbar").classList.remove("is-fixed-top");
		}
	});
});
