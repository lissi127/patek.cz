---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: false
authors: [ "unknown" ]
when: {{ .Date }}
slides:
  - data: <url>
recordings:
  - data: <url>
---